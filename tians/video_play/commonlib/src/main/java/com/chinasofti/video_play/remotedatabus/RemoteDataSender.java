package com.chinasofti.video_play.remotedatabus;

import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.bundle.ElementName;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteObject;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteException;

import java.util.HashMap;
import java.util.Map;

/**
 * CommRemoteProxy class for remote Object, all remote communication will go through this proxy instance.
 */
public class RemoteDataSender {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "RemoteDataSender");
    private static final int ERROR = -1;
    private static final Map<String, RemoteConnection> REMOTE_CONNECTION_MAP = new HashMap<>();

    /**
     * This method will be invoke by devices' DistributeInternalAbility when
     * it want to connect with remote device.
     *
     * @param deviceId the deviceID of which device we want to transfer to.
     * @param context  native ability context
     * @return isSuccess whether connect remote is successful.
     */
    public static boolean connectRemote(String deviceId, Context context) {
        RemoteConnection remote = REMOTE_CONNECTION_MAP.get(deviceId);
        if (remote != null) {
            context.disconnectAbility(remote);
            REMOTE_CONNECTION_MAP.remove(deviceId);
        }

        RemoteConnection newRemote = new RemoteConnection();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId(deviceId)
                .withBundleName(context.getBundleName())
                .withAbilityName(RemoteDataReceiverAbility.class.getName())
                .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                .build();

        intent.setOperation(operation);
        boolean isSuccess = context.connectAbility(intent, newRemote);
        HiLog.error(TAG, "connectRemote " + isSuccess);
        if (isSuccess) {
            REMOTE_CONNECTION_MAP.put(deviceId, newRemote);
        }
        return isSuccess;
    }

    /**
     * This method will be invoke by devices' DistributeInternalAbility when
     * it want to disconnect with remote device.
     *
     * @param deviceId the deviceID of which device we want to stop connection.
     * @param context  native ability context
     */
    public static void disConnectRemote(String deviceId, Context context) {
        RemoteConnection remote = REMOTE_CONNECTION_MAP.get(deviceId);
        if (remote != null) {
            context.disconnectAbility(remote);
            REMOTE_CONNECTION_MAP.remove(deviceId);
        }
    }

    /**
     * this method will be invoke by phone DistributeInternalAbility when it wants to
     * send message to other remote device.
     *
     * @param deviceId the deviceID of remote device which we send message to.
     * @param code     device message code.
     * @param message  the action code.
     * @return result of sending message.
     */
    public static int sendMessage(String deviceId, int code, int message) {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption(MessageOption.TF_SYNC);
        data.writeInt(message);
        RemoteConnection remote = REMOTE_CONNECTION_MAP.get(deviceId);
        int result = ERROR;
        if (remote != null) {
            boolean success = remote.sendMessage(code, data, reply, option);
            HiLog.info(TAG, "RemoteDataSender: sendMessage " + success);
            if (success) {
                result = reply.readInt();
            }
            data.reclaim();
            reply.reclaim();
        }
        return result;
    }

    private static class RemoteConnection implements IAbilityConnection {
        IRemoteObject myRemoteProxy;
        boolean isConnected = false;

        RemoteConnection() {
        }

        @Override
        public void onAbilityConnectDone(ElementName element, IRemoteObject remote, int resultCode) {
            // when remote device connect done, it return a serializable object represent remote object
            // we can control remote object through this proxy
            myRemoteProxy = remote;
            isConnected = true;
            HiLog.info(TAG, "RemoteDataSender: onAbilityConnectDone");
        }

        @Override
        public void onAbilityDisconnectDone(ElementName element, int resultCode) {
            // when remote device PA is closed this callback will be called
            // developer can handle PA lifecycle according to different returned error message
            isConnected = false;
            myRemoteProxy = null;
        }

        boolean sendMessage(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
            try {
                myRemoteProxy.sendRequest(code, data, reply, option);
            } catch (RemoteException e) {
                HiLog.error(TAG, "SendCommand error");
                return false;
            }
            return true;
        }
    }
}
