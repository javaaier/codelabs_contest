import router from '@system.router';
import device from '@system.device';
import prompt from '@system.prompt';
export default {
    data: {
        msg:"",
        isMsg:false,
        isZan:false,
        isShc:false,
        isJubao:false,
        zanSrc:"/common/images/zan1.png",
        shcSrc:"/common/images/share.png",
        msgArr:[
            {
                id:0,
                msg: "哈哈",
                name:"靓仔:",
                jubao:"",

            },
            {
                id:1,
                msg:"最美靓仔",
                name:"旭日之风:",
                jubao:"",
                child:[]
            },
            {
                id:2,
                msg: "天下之大,何处藏身",
                name:"风一样的美男子:",
                jubao:"",
                child:[]
            }
        ]
    },
    props:{
      // 视频路径
      src:{
          default:[],
      },
      // 是否显示工具栏
      is_controls:{
          default:false
      },
      // 组件布局方式
      direction:{
        default:"adapt"
      },


    },
    onInit() {
    },
    // 事件:视频准备完成
    prepared(duration){
        this.$emit("prepared",duration);
    },
    // 事件:播放时触发
    start(){
        this.$emit("start","start");

    },
    // 事件:视频暂停时候触发
    pause(){
        this.$emit("pause","pause");
    },
    // 事件:视频播放结束时触发
    finish(){
        this.$emit("finish","finish");
    },
    // 事件:播放失败时触发
    error(){
        this.$emit("error","error")
    },
    // 事件:视频进度发生变化时触发
    timeupdate(time){
        this.$emit("timeupdate",time)
    },
    // 事件:进入或退出全屏时触发
    fullscreenchange(fullscreen){
        this.$emit("fullscreenchange",fullscreen)
    },
    // 事件:视频停止播放时候触发
    stop(){
        this.$emit("stop","stop")
    },
    msgChange(e){
        this.msg=e.value;
    },
    send(){
        if(this.msg!=""){
            this.msgArr.unshift({
                name:"我:",
                msg:this.msg
            });
            this.msg="";
        }
    },
    pinglun(){
        this.isMsg=true;
        console.info("isMsg:"+this.isMsg)
    },
    videoClick(){
        this.isMsg=false;
    },
    zan(){
        this.isZan=!this.isZan;
        if(this.isZan==true){
            this.zanSrc="/common/images/zan2.png"
        }else{
            this.zanSrc="/common/images/zan1.png"
        }
    },
    shc(){
        this.isShc=!this.isShc;
    },
    longpress(idx){
        this.msgArr[idx].jubao="举报";
        prompt.showToast({
            message: JSON.stringify(this.msgArr[idx]),
            duration: 2000,
        });
    },
    jubao(idx){
        this.msgArr[idx].jubao=""
    },
    // 分布式流转
    async transfer(index,item) {
        var forwardCompatible = false;
        device.getInfo({
            success: function (data) {
                if (data.apiVersion >= 5) {
                    forwardCompatible = false;
                } else {
                    forwardCompatible = true;
                }
            },
            fail: function (data, code) {
                console.error("index.js get Device API version failed");
            }
        });
        const target = {};
        target.bundleName = 'com.chinasofti.video_play';
        target.abilityName = 'DeviceSelectAbility';
        target.deviceType = 1;
        target.data = {
            title: "",
            transferName: 'HUAWEI News',
            transferIcon: "",
        };
        this.deviceId = '';
        var res = await FeatureAbility.startAbilityForResult(target);
        if (!!forwardCompatible == true) {
            res = JSON.parse(res);
        }
        console.info("res.code:"+JSON.stringify(res))
        if (res.code == 0) {
            var dataRemote = JSON.parse(res.data);
            var data;
            if (!!forwardCompatible == true) {
                data = dataRemote;
            } else {
                data = dataRemote.result;
            }
            const targetDeviceId = data.deviceId;
            const targetDeviceType = data.deviceType;
            const deviceDebugMode = data.deviceDebugMode;
            if (!!targetDeviceId && (targetDeviceId != this.deviceId)) {
                this.deviceId = targetDeviceId;
                const target = {};
                target.bundleName = 'com.chinasofti.video_play';
                target.abilityName = 'MainAbility';
                target.deviceId = this.deviceId;
                target.data = {
                    newsUrl: "pages/detail/detail",
                    onTransfer: true,
                    video: item,
                    isMsg:this.isMsg,
                    isZan:this.isZan,
                };
                await FeatureAbility.startAbility(target);
                if (deviceDebugMode == true) {
                    await FeatureAbility.startAbility(target);
                } else {
                    prompt.showToast({
                        message: 'Video News can only be transferred to TV',
                        duration: 2000,
                    });
                }

            }
        }
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        console.info("saveData:"+JSON.stringify(saveData))
    }

}
