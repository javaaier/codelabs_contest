// @ts-nocheck
import prompt from '@system.prompt'

export default {
    data: {
        videoList: [
            {
                id: 1,
                url: '/common/images/2.mp4',
                sc: false,
                dz: true,
                pl: [
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HDC2021'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: '我期待 HarmonyOS越来越好'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HarmonyOS 加油'
                    }
                ]
            },
            {
                id: 2,
                url: '/common/images/1.MP4',
                sc: false,
                dz: false,
                pl: [
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HDC2021'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: '我期待 HarmonyOS越来越好'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HarmonyOS 加油'
                    }
                ]
            },
            {
                id: 3,
                url: '/common/images/3.mp4',
                sc: true,
                dz: true,
                pl: [
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HDC2021'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: '我期待 HarmonyOS越来越好'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HarmonyOS 加油'
                    }
                ]
            },
            {
                id: 4,
                url: '/common/images/4.MP4',
                sc: true,
                dz: true,
                pl: [
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HDC2021'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: '我期待 HarmonyOS越来越好'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HarmonyOS 加油'
                    }
                ]
            },
            {
                id: 5,
                url: '/common/images/5.MP4',
                sc: false,
                dz: false,
                pl: [
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HDC2021'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: '我期待 HarmonyOS越来越好'
                    },
                    {
                        img: '/common/images/tx.png',
                        name: '王浪浪',
                        value: 'HarmonyOS 加油'
                    }
                ]
            }
        ],// 视频数据
        inputValue: '',// 评论输入框数据
        showPl: false,// 是否展示评论
        swiperIndex: 0,// 当前第几个视频
        deviceId: '',
        isTransmit: false,// 是否流转
        marqueeList: [
            {
                show: false,
                value: ''
            },
            {
                show: false,
                value: ''
            },
            {
                show: false,
                value: ''
            }
        ],// 跑马灯评论数据
        isShow: false // 是否播放
    },
    onReady(){
//        生成跑马灯数据
        const l = this.videoList[this.swiperIndex].pl.length;
        for (var index = 0; index < l; index++) {
            const num = Math.random() * 50000 * (index + 1)
            if (index === 0) num = 2000
            setTimeout(() => {
                this.randerMarquee()
            }, num)
        }
    },
//    控制视频是否播放
    controlsClick(id) {
        if (this.isShow) {
            this.$element(id).start();
            prompt.showToast({
                message: '视频已播放',
                duration: 3000,
            });
        } else {
            this.$element(id).pause();
            prompt.showToast({
                message: '视频已暂停',
                duration: 3000
            });
        }
        this.isShow = !this.isShow
    },
//    添加评论后触发
    changeInput(e) {
        const l = this.videoList[this.swiperIndex].pl.length;
        for (var index = 0; index < l; index++) {
            const num = Math.random() * 50000 * (index + 1)
            if (index === 0) num = 2000
            setTimeout(() => {
                this.randerMarquee()
            }, num)
        }
        this.inputValue = e.value
    },
//    点击发送 后触发
    buttonClick(index) {
        const data = {
            name: '我',
            img: '/common/images/txnv.png',
            value: this.inputValue
        }
        this.videoList[index].pl.push(data)
        this.randerMarquee(this.inputValue)
    },
//    生成随机跑马灯数据
    randerMarquee(value) {
        const index = Math.floor(Math.random() * 3)
        const i = Math.floor(Math.random() * this.videoList[this.swiperIndex].pl.length)
        if (this.marqueeList[index].show === false) {
            this.marqueeList[index].show = true
            this.marqueeList[index].value = value ? value : this.videoList[this.swiperIndex].pl[i].value
            if (value) this.inputValue = ''
        } else {
            this.randerMarquee()
        }
    },
//    跑马灯完成展示触发
    finish(index) {
        this.marqueeList[index].show = false
        this.marqueeList[index].value = ''
    },
//    收藏和点赞
    imgClick(type, index) {
        this.videoList[index][type] = !this.videoList[index][type]
        const msg = type === 'sc' ? '收藏' : '点赞'
        prompt.showToast({
            message: '点击了' + msg,
            duration: 2000
        });
    },
//    展示评论
    plClick() {
        this.showPl = !this.showPl
    },
    swiperChange(e) {
        this.swiperIndex = e.index;
    },
//    流转
    async transfer() {
        const target = {};
        target.bundleName = 'com.example.videoplayer';
        target.abilityName = 'DeviceSelectAbility';
        target.deviceType = 1;
        this.deviceId = '';
        var result = await FeatureAbility.startAbilityForResult(target);
        if (result.code == 0) {
            var dataRemote = JSON.parse(result.data);
            var data;
            data = dataRemote.result;
            const targetDeviceId = data.deviceId;
            if (!!targetDeviceId && (targetDeviceId != this.deviceId)) {
                this.deviceId = targetDeviceId;
                const target = {};
                target.bundleName = 'com.example.videoplayer';
                target.abilityName = 'MainAbility';
                target.deviceId = this.deviceId;
                target.data = {
                    swiperIndex: this.swiperIndex,
                    isTransmit: true
                };
                var res1 = await FeatureAbility.startAbility(target);
                if (res1.code === 0) {
                    prompt.showToast({
                        message: '流转成功, 即将关闭本应用',
                        duration: 3000,
                    });
                    setTimeout(() => {
                        FeatureAbility.finishWithResult(100);
                    }, 6000)
                }
            }
        }
    },
//    流转过来会触发
    async onNewRequest() {
        if (this.isTransmit === true) {
            this.$element('video-content').swipeTo({index: this.swiperIndex});
        }
    }
}