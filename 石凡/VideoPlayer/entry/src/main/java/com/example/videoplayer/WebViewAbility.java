package com.example.videoplayer;

import com.example.videoplayer.newsdata.NewsInfo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

import ohos.agp.components.Button;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.webengine.BrowserAgent;
import ohos.agp.components.webengine.Navigator;
import ohos.agp.components.webengine.WebAgent;
import ohos.agp.components.webengine.WebView;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import ohos.utils.zson.ZSONObject;

/**
 * WebViewAbility implements Web Page browsing function in the app.
 */
public class WebViewAbility extends Ability {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "WebViewAbility");
    private static final String SHARING_EXTRA_INFO = "sharing_fa_extra_info";
    private static final String INIT_WEB_PAGE = "web_page";
    private static final String INIT_INDEX = "index";
    private WebView webView;
    private ProgressBar progressBar;
    private Button buttonBack;
    private Button buttonTrans;
    private String webPage;
    private int newsIndex;
    private NewsInfo newsInfo;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_webview_detail);
        initComponent();
        newsInfo = NewsInfo.getInstance(this);

        IntentParams intentParams = intent.getParams();
        if (intentParams.hasParam("__startParams")) {
            ZSONObject newZson = ZSONObject.stringToZSON(intentParams.getParam("__startParams").toString());
            this.webPage = newZson.getString(INIT_WEB_PAGE);
            this.newsIndex = Integer.parseInt(newZson.getString(INIT_INDEX));
        }

        String shareInfo = intent.getStringParam(SHARING_EXTRA_INFO);
        if (shareInfo != null) {
            this.newsIndex = Integer.parseInt(shareInfo);
            this.webPage = newsInfo.getWebNewsUrl(this.newsIndex);
        }

        configComponent();
    }

    @Override
    public void onNewIntent(Intent intent) {
        String shareInfo = intent.getStringParam(SHARING_EXTRA_INFO);
        if (shareInfo != null) {
            this.newsIndex = Integer.parseInt(shareInfo);
            this.webPage = newsInfo.getWebNewsUrl(this.newsIndex);
        }
        webView.load(this.webPage);
        super.onNewIntent(intent);
    }

    private void initComponent() {
        this.webView = (WebView) findComponentById(ResourceTable.Id_webview);
        this.progressBar = (ProgressBar) findComponentById(ResourceTable.Id_progressbar);
        this.buttonBack = (Button) findComponentById(ResourceTable.Id_button_back);
        this.buttonTrans = (Button) findComponentById(ResourceTable.Id_button_transfer);
    }

    private void configComponent() {
        webView.load(this.webPage);
        webView.setWebAgent(new WebAgent());
        webView.getWebConfig().setJavaScriptPermit(true);
        webView.setBrowserAgent(new BrowserAgent(this) {
            @Override
            public void onProgressUpdated(WebView webView, int newProgress) {
                super.onProgressUpdated(webView, newProgress);
                if (newProgress == 100) {
                    progressBar.setVisibility(WebView.HIDE);
                } else {
                    progressBar.setVisibility(WebView.VISIBLE);
                    progressBar.setProgressValue(newProgress);
                }
            }
        });
        Navigator navigator = webView.getNavigator();
        if (navigator.canGoBack()) {
            navigator.goBack();
        }
        if (navigator.canGoForward()) {
            navigator.goForward();
        }

        this.buttonBack.setClickedListener(component -> {
            if (navigator.canGoBack()) {
                navigator.goBack();
            } else {
                onBackPressed();
            }
        });

        this.buttonTrans.setClickedListener(component -> {
            Intent newIntent = new Intent();
            newIntent.setParam("title", this.webPage);
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(DeviceSelectAbility.class.getName())
                    .build();
            newIntent.setOperation(operation);
            startAbilityForResult(newIntent, 0);
        });
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultData == null) {
            return;
        }
        super.onAbilityResult(requestCode, resultCode, resultData);

        ZSONObject newZson = ZSONObject.classToZSON(resultData);
        String deviceName = newZson.getZSONObject("params").getZSONObject("params").getZSONObject("resultData")
                .getZSONObject("result").getString("deviceName");
        String deviceType = newZson.getZSONObject("params").getZSONObject("params").getZSONObject("resultData")
                .getZSONObject("result").getString("deviceType");
        String deviceId = newZson.getZSONObject("params").getZSONObject("params").getZSONObject("resultData")
                .getZSONObject("result").getString("deviceId");
        if (deviceId == null || "".equals(deviceId)) {
            return;
        }
        boolean debugMode = newZson.getZSONObject("params").getZSONObject("params").getZSONObject("resultData")
                .getZSONObject("result").getBoolean("deviceDebugMode");

        Intent newIntent = new Intent();
        Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder().withDeviceId(deviceId)
                .withBundleName("com.example.videoplayer").withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE);
        Operation operation;

        // when transferring to smartphone which simulated as tv, add debugMode here
        if ("SMART_TV".equals(deviceType)) {
            newIntent.setParam("newsUrl", newsInfo.getNewsUrl(this.newsIndex));
            operation = operationBuilder.withAbilityName(MainAbility.class.getName()).build();
            newIntent.setOperation(operation);
            startAbility(newIntent);

            Intent localIntent = new Intent();
            localIntent.setParam("newsUrl", newsInfo.getNewsUrl(this.newsIndex));
            localIntent.setParam("newsTitle", newsInfo.getNewsTitle(this.newsIndex));
            localIntent.setParam("newsImage", newsInfo.getNewsImage(this.newsIndex));
            localIntent.setParam("deviceId", deviceId);
            localIntent.setParam("deviceType", deviceType);
            localIntent.setParam("deviceName", deviceName);

            Operation localOpt = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("com.example.videoplayer")
                    .withAbilityName(MainAbility.class.getName())
                    .build();
            localIntent.setOperation(localOpt);
            startAbility(localIntent);
        } else if ("SMART_PHONE".equals(deviceType)) {
            newIntent.setParam(SHARING_EXTRA_INFO, String.valueOf(this.newsIndex));
            operation = operationBuilder.withAbilityName(WebViewAbility.class.getName()).build();
            newIntent.setOperation(operation);
            startAbility(newIntent);
        } else {
            HiLog.error(TAG, "Device Type Error, please Try Again");
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
