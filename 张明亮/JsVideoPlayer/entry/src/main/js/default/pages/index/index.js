/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import prompt from '@system.prompt';

export default {
    data: {
        autoplay: false, // 是否自动播放
        videoId: 'video', // 播放器id
        url: '/common/video/s1.mp4', // 视频地址
        posterUrl: '/common/images/bg-tv.jpg', // 视频预览的海报路径
        controlShow: true, // 是否显示控制栏
        loop: true, // 是否循环播放
        startTime: 10, // 播放开始时间
        speed: 0.2, // 播放速度
        isfullscreenchange: false, // 是否全屏
        input:'', // 获取输入框的内容
        dataList:[
            {
                url: '/common/video/huawei-watch-gt2-pro-video.mp4',
                comments:[
                {
                    src:'/common/images/001.jpg',
                    name:'哈莫呢哦艾斯',
                    date:'10-9',
                    commend:'第二期不错，赶快冲一波~',
                },
                {
                    src:'/common/images/002.gif',
                    name:'137******82',
                    date:'10-9',
                    commend:'支持支持！！！！！！！！',
                },
                {
                    src:'/common/images/003.jpg',
                    name:'FIEforever',
                    date:'10-9',
                    commend:'严重支持一波，冲……',
                },
                ],
            },
            {
                url: '/common/video/mate40-pv.mp4',
                comments:[
                    {
                        src:'/common/images/002.gif',
                        name:'132******34',
                        date:'10-9',
                        commend:'你好棒！！！！！！！！！！！！',
                    },
                    {
                        src:'/common/images/004.jpg',
                        name:'哈莫呢哦艾斯',
                        date:'10-9',
                        commend:'一起冲，我看这架势，后面还有好玩的可以继续冲',
                    },
                    {
                        src:'/common/images/005.png',
                        name:'云司机',
                        date:'10-8',
                        commend:'电脑配置不行，还装着win7',
                    },
                ],
            },
        ],
        page:0, // 当前页面
    },
//    onInit() {
//        this.page = 0
////        7913588,
////        7913535
//    },

    // 分布式调度
    transfer: async function() {
        // 应用进行迁移
        let result = await FeatureAbility.continueAbility();
        console.info("result:" + JSON.stringify(result));
    },
    onStartContinuation() {
        // 判断当前的状态是不是适合迁移
        console.info("onStartContinuation");
        return true;
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        console.info("CompleteContinuation: code = " + code);
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = this.continueAbilityData;
        Object.assign(saveData, data)
    },
    //  迁移完成恢复数据
    onRestoreData(restoreData) {
        // 收到迁移数据，恢复。
        this.continueAbilityData = restoreData;
    },

    // 获取当前页
    changePage(obj) {
        this.page = obj.index
        console.info("this.page = " + JSON.stringify(this.page))
    },
    // 获取输入内容
    changeValue(e) {
        this.input = e.value
    },
    // 发送评论
    send() {
        console.info('this.page = ' + JSON.stringify(this.page))
        this.dataList[this.page].comments.push({
            src:'/common/images/000.jpg',
            name:'liangzili',
            date:'10-14',
            commend:this.input,
        })
        this.input = ''
    },


    // 视频准备完成时触发该事件
    prepared(e) {
        this.showPrompt('视频时长：' + e.duration + '秒');
    },
    // 视频开始播放
    start() {
        this.showPrompt('视频开始播放');
    },
    // 视频暂停播放
    pause() {
        this.showPrompt('视频暂停播放');
    },
    // 视频播放完成
    finish() {
        this.$element('confirmDialog').show();
    },
    // 拖动进度条调用
    seeked(e) {
        this.showPrompt('设置播放进度：' + e.currenttime + '秒');
    },
    // 播放进度变化调用
    timeupdate(e) {

    },
    // 自带组件进入全屏和退出全屏
    fullscreenchange(e) {
        if (e.fullscreen === 1) {
            this.isfullscreenchange = true;
        } else {
            this.isfullscreenchange = false;
        }
    },
    // 长按屏幕视频进入和退出全屏调用
    longPressFullscreenchange() {
        if (this.isfullscreenchange) { // 全屏
            this.$element('video').exitFullscreen();
            this.isfullscreenchange = false;
        } else { // 非全屏
            this.$element('video').requestFullscreen({ screenOrientation: 'default' });
            this.isfullscreenchange = true;
        }
    },

    // dialog确定
    confirm() {
        this.$element('video').start();
        this.$element('confirmDialog').close();
    },
    // dialog取消
    cancel() {
        this.$element('confirmDialog').close();
    },
    // 弹框
    showPrompt(msg) {
        prompt.showToast({
            message: msg,
            duration: 1000
        });
    },
    // 点击视频
    hideControls() {
        this.controlShow = !this.controlShow;
    }

};
