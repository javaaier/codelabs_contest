package com.suimdream.codelab2.player.view;

import com.suimdream.codelab2.player.core.PlayerStatus;

/**
 * Playback status listener.
 */
public interface IPlayStatusListener {
    /**
     * Update of playback status.
     *
     * @param status the next playback status.
     */
    void onPlayStatusChange(PlayerStatus status);
}
