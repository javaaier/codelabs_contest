package com.suimdream.codelab2.model;

import com.suimdream.codelab2.constant.Constants;
import com.suimdream.codelab2.utils.PathUtils;

/**
 * Resolution Model Class
 */
public class ResolutionModel {
    private String name;

    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
