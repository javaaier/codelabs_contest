// @ts-nocheck
import app from '@system.app';
export default {
    data: {
        img: "resources/media/pic_tv.png",
        continueAbilityData: {
            currentIndex: 0,
            videoList: [
                "/common/000001.mp4",
                "/common/000002.mp4",
                "/common/000003.mp4"
            ],
            timeupdatetime: 2,
            isStart: true,
            todolist: [
                {title: 'HDC2021活动门票进行中'},
                {title: '我期待HarmonyOS生态越来越完善'},
                {title: 'HarmonyOS你值得拥有'}],
            comment: ''
        }
    },
    onInit() {
    },
    changeSwiper(e) {
        console.info("onRestoreData:changeSwiper");
        this.switchPlay(e.index);
    },
    switchPlay(index) {
        console.info("onRestoreData:onShow <> " + index);
        this.continueAbilityData.currentIndex = index;
        if(index == 0) {
            this.$element('videoOne').start();
            this.$element('videoTwo').pause();
            this.$element('videoThree').pause();
            console.info("onRestoreData:videoOne <> start " + this.$element('videoOne').starttime);
        }else if(index == 1) {
            this.$element('videoOne').pause();
            this.$element('videoTwo').start();
            this.$element('videoThree').pause();
            console.info("onRestoreData:videoTwo <> start " + this.$element('videoTwo').starttime);
        }else if(index == 2) {
            this.$element('videoOne').pause();
            this.$element('videoTwo').pause();
            this.$element('videoThree').start();
            console.info("onRestoreData:videoThree <> start " + this.$element('videoThree').starttime);
        }
    },

    //流转事件
    tryContinueAbility: async function() {
        // 应用进行迁移
        let result = await FeatureAbility.continueAbility();
        console.info("result:" + JSON.stringify(result));
    },
    onStartContinuation() {
        // 判断当前的状态是不是适合迁移
        console.info("onStartContinuation");
        return true;
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        console.info("CompleteContinuation: code = " + code);
        app.terminate();

    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = this.continueAbilityData;
        console.info("onSaveData:" + JSON.stringify(data));
        Object.assign(saveData, data)
    },
    onRestoreData(restoreData) {
        console.info("onRestoreData:" + JSON.stringify(restoreData));
        // 收到迁移数据，恢复。
        this.continueAbilityData = restoreData;

        var currentIndex = this.continueAbilityData.currentIndex;
        var currentTime = this.continueAbilityData.timeupdatetime;

        this.$element('videoOne').pause();
        this.$element('videoTwo').pause();
        this.$element('videoThree').pause();

        this.$element('videoOne').starttime = currentTime;
        this.$element('videoTwo').starttime = currentTime;
        this.$element('videoThree').starttime = currentTime;

        console.info("onRestoreData*index: " + currentIndex + ", *currentTime: " + currentTime);
//        if(currentIndex == 0) {
//            this.$element('videoOne').starttime = currentTime;
////            this.$element('videoOne').setCurrentTime({currenttime: currentTime});
////            this.$element('videoOne').start();
//        }else if(currentIndex == 1) {
//            this.$element('videoTwo').starttime = currentTime;
////            this.$element('videoTwo').setCurrentTime({currenttime: currentTime});
////            this.$element('videoTwo').start();
//        }else if(currentIndex == 2) {
//            this.$element('videoThree').starttime = currentTime;
////            this.$element('videoThree').setCurrentTime({currenttime: currentTime});
////            this.$element('videoThree').start();
//        }
        this.switchPlay(currentIndex);
    },

    //评论事件
    showPanel() {
        this.$element('simplepanel').show()
    },
    closePanel() {
        this.$element('simplepanel').close()
    },
    changeValue(e) {
        this.continueAbilityData.comment = e.value;
    },
    enterkeyClick(e) {
        this.continueAbilityData.todolist.push({title: this.continueAbilityData.comment});
        this.continueAbilityData.comment = "";
    },
    timeupdateCallback:function(e){ this.continueAbilityData.timeupdatetime = e.currenttime;},
    change_start_pause_one: function() {
        if(this.continueAbilityData.isStart) {
            this.$element('videoOne').pause();
            this.continueAbilityData.isStart = false;
        } else {
            this.$element('videoOne').start();
            this.continueAbilityData.isStart = true;
        }
    },
    change_start_pause_two: function() {
        if(this.continueAbilityData.isStart) {
            this.$element('videoTwo').pause();
            this.continueAbilityData.isStart = false;
        } else {
            this.$element('videoTwo').start();
            this.continueAbilityData.isStart = true;
        }
    },
    change_start_pause_three: function() {
        if(this.continueAbilityData.isStart) {
            this.$element('videoThree').pause();
            this.continueAbilityData.isStart = false;
        } else {
            this.$element('videoThree').start();
            this.continueAbilityData.isStart = true;
        }
    }
}
