/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.code.distributedvideo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.PageSlider;
import ohos.code.distributedvideo.MainAbility;
import ohos.code.distributedvideo.PlayerAbility;
import ohos.code.distributedvideo.ResourceTable;
import ohos.code.distributedvideo.data.VideoListMo;
import ohos.code.distributedvideo.provider.AdvertisementProvider;
import ohos.code.distributedvideo.provider.CommonProvider;
import ohos.code.distributedvideo.provider.ViewProvider;
import ohos.code.distributedvideo.util.LogUtil;
import ohos.code.distributedvideo.util.MediaUtil;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * the main page
 *
 * @since 2020-12-04
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = MainAbility.class.getSimpleName();
    private static final String INTENT_STARTTIME_PARAM = "intetn_starttime_param";
    private CommonProvider<VideoListMo> commonProvider;
    private AdvertisementProvider<Component> advertisementProvider;
    private PageSlider advPageSlider;
    private ScheduledExecutorService scheduledExecutor;
    private int currentPageIndex = 0;
    private Runnable slidTask;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_video);
        initContent();
    }

    private void initContent() {
        initScheduledExecutor();
        initList();
    }

    private void initScheduledExecutor() {
        slidTask = () -> getUITaskDispatcher().asyncDispatch(() -> {
            if (currentPageIndex == advertisementProvider.getCount()) {
                currentPageIndex = 0;
            } else {
                currentPageIndex++;
            }
            advPageSlider.setCurrentPage(currentPageIndex);
        });
        scheduledExecutor = new ScheduledThreadPoolExecutor(1);
    }

    private void initList() {
        commonProvider = new CommonProvider<VideoListMo>(
                MediaUtil.getPlayListMos(),
                getContext(),
                ResourceTable.Layout_recommend_gv_item) {
            @Override
            protected void convert(ViewProvider holder, VideoListMo item, int position) {
                holder.setText(ResourceTable.Id_name_tv, item.getName());
                holder.setText(ResourceTable.Id_content_tv, item.getDescription());
                holder.setImageResource(ResourceTable.Id_image_iv, item.getSourceId());
            }
        };
        ListContainer playListContainer = null;
        if (findComponentById(ResourceTable.Id_video_list_play_view) instanceof ListContainer) {
            playListContainer = (ListContainer) findComponentById(ResourceTable.Id_video_list_play_view);
        }
        playListContainer.setItemProvider(commonProvider);
        playListContainer.setItemClickedListener((listContainer, component, index, position) -> startFa());
    }

    private void startFa() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName())
                .withAbilityName(PlayerAbility.class.getName()).build();
        intent.setOperation(operation);
        intent.setParam(INTENT_STARTTIME_PARAM, 0);
        startAbility(intent);
    }
    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onInactive() {
        super.onInactive();
        LogUtil.info(TAG, "is onInactive");
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        LogUtil.info(TAG, "is onForeground");
    }

    @Override
    public void onBackground() {
        super.onBackground();
        LogUtil.info(TAG, "is onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        LogUtil.info(TAG, "is onStop");
    }

    @Override
    protected void onBackPressed() {
        LogUtil.info(TAG, "is onBackPressed");
        super.onBackPressed();
    }
}
