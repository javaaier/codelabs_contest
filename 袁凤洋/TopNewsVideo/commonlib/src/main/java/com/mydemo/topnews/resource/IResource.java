package com.mydemo.topnews.resource;

/**
 * Define the Interface for getting resource path.
 */
public interface IResource {
    String getResourcePath();
}
