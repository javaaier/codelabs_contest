package com.mydemo.topnews.constant;

/**
 * Defines the routing action for page.
 */
public class RouteRegister {
    /**
     * Route to VideoPlayAbilitySlice
     */
    public static final String SLICE_SAMPLE = "action.sample.slice";
}
