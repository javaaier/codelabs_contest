package com.cxs.videoplayer.constant;

public enum ControllCode {

    START(1000),

    PLAY(1001),

    PLAYING(1002),

    PAUSE(1003),
    ;

    private final int code;

    ControllCode(int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }

    public static ControllCode getByValue(int value) {
        for (ControllCode controllCode : ControllCode.values()) {
            if (controllCode.code == value) {
                return controllCode;
            }
        }
        throw new IllegalArgumentException("No element matches " + value);
    }

}
