package com.cxs.videoplayer.util;

import ohos.app.Context;
import ohos.data.distributed.common.*;
import ohos.data.distributed.user.SingleKvStore;

public class KvStoreUtils {
    public static String STORE_ID = "my_app";
    private KvStoreUtils () {}

    /**
     * 创建分布式数据库管理器实例KvManager
     * @param context
     * @return
     */
    public static KvManager createManager(Context context) {
        KvManager manager = null;
        try {
            // 根据应用上下文创建KvManagerConfig对象
            KvManagerConfig config = new KvManagerConfig(context);
            // 创建分布式数据库管理器实例
            manager = KvManagerFactory.getInstance().createKvManager(config);
        } catch (KvStoreException e) {
            System.out.println(e.getMessage());
        }
        return manager;
    }

    /**
     * 创建SINGLE_VERSION分布式数据库
     * @param kvManager
     * @return
     */
    public static SingleKvStore createDB(KvManager kvManager) {
        SingleKvStore kvStore = null;
        try {
            // 数据库配置
            Options options = new Options();
            /*
                setCreateIfMissing：设置数据库不存在时是否创建
                setEncrypt： 设置数据库是否加密
                setKvStoreType： 设置分布式数据库的类型
                setAutoSync： 设置数据库是否自动同步（默认true）
             */
            options.setCreateIfMissing(true)
                    .setEncrypt(false)
                    .setKvStoreType(KvStoreType.SINGLE_VERSION);
            // 通过分布式数据库管理器实例获取数据库
            kvStore = kvManager.getKvStore(options, STORE_ID);
        } catch (KvStoreException e) {
            System.out.println(e.getMessage());
        }
        return kvStore;
    }

}