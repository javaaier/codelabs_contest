package com.cxs.videoplayer.data;

public class DeviceInfo {
    String id;
    String type;

    public DeviceInfo(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
