package com.cxs.videoplayer.component;

import com.cxs.videoplayer.ResourceTable;
import com.cxs.videoplayer.data.VideoInfo;
import com.cxs.videoplayer.player.IHmPlayer;
import com.cxs.videoplayer.provider.VideoListProvider;
import com.cxs.videoplayer.util.VideoUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.accessibility.ability.SoftKeyBoardListener;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.List;

public class VideoListView extends DirectionalLayout {

    Context context;
    IHmPlayer iPlayer;
    static List<VideoInfo> videoInfos;
    final static long START_MILLI_TIME = 0;

    public VideoListView(Context context, IHmPlayer iPlayer) {
        super(context);
        this.context = context;
        this.iPlayer = iPlayer;
    }

    public DirectionalLayout getView() {
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_video_list, null, false);
        ListContainer listContainer = (ListContainer) layout.findComponentById(ResourceTable.Id_video_listcontainer);

        VideoListProvider videoListProvider = new VideoListProvider(getData());
        iPlayer.setVideoList(videoInfos);
        videoListProvider.setVideoClickedListener(new VideoListProvider.ItemClickedListener() {
            @Override
            public void onClicked(VideoInfo videoInfo) {
                iPlayer.setVideo(videoInfo);
                iPlayer.start(START_MILLI_TIME);
            }
        });
        listContainer.setItemProvider(videoListProvider);
        return layout;
    }

    /**
     * 获取视频列表
     * @return
     */
    private List<VideoInfo> getData() {
        videoInfos = VideoUtils.getVideoInfo(context);
        return videoInfos;
    }

    public static List<VideoInfo> getVideoInfos() {
        return videoInfos;
    }

}
