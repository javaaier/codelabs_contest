/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzf.mytestnews.ability;

import com.yc.video.ui.view.BasisVideoController;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.bundle.AbilityInfo;
import ohos.sysappcomponents.settings.SystemSettings;

/**
 * Ability基类
 *
 * @noinspection
 * @since 2021-05-10
 */
public abstract class BaseAbility extends FractionAbility {
    /**
     * 基本视频控制器
     */
    public BasisVideoController mController;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(getLayoutResId());
        initComponent(intent);
        initListener();
    }

    /**
     * 初始化视图
     *
     * @param intent 意图
     */
    protected void initComponent(Intent intent) {
    }

    /**
     * 初始化监听
     */
    protected void initListener() {
    }

    /**
     * 获取layout对应的resID
     *
     * @return ResId
     */
    protected abstract int getLayoutResId();

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        if (mController.getFullButton() != null) {
            mController.setFullScreenButton(mController.getFullButton());

            DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(this);
            String value = SystemSettings.getValue(dataAbilityHelper,
                SystemSettings.General.ACCELEROMETER_ROTATION_STATUS);
            if ("1".equals(value)) {
                mController.orientationChanged(displayOrientation);
            }
        }
    }
}
