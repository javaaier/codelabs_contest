/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzf.mytestnews.provider;

import com.bumptech.glide.Glide;
import com.lxj.xpopup.XPopup;
import com.lzf.mytestnews.ResourceTable;
import com.lzf.mytestnews.bean.NewsInfo;
import com.lzf.mytestnews.utils.ZhihuCommentPopup;
import com.lzf.mytestnews.view.TikTokView;
import com.yc.videocache.cache.PreloadManager;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Tiktok2Adapter
 *
 * @since 2021-05-12
 */
public class VideoListAdapter extends PageSliderProvider {
    /**
     * View缓存池，从ViewPager中移除的item将会存到这里面，用来复用
     */
    private List<Component> mViewPool = new ArrayList<>();

    /**
     * 数据源
     */
    private List<NewsInfo> mVideoBeans;

    private Context mContext;

    /**
     * 构造函数
     *
     * @param videoBeans 视频信息
     */
    public VideoListAdapter(Context context, List<NewsInfo> videoBeans) {
        this.mVideoBeans = videoBeans;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mVideoBeans == null ? 0 : mVideoBeans.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer container, int position) {
        Context context = container.getContext();
        Component view = null;
        if (mViewPool.size() > 0) {
            // 取第一个进行复用
            view = mViewPool.get(0);
            mViewPool.remove(0);
        }

        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_item_tik_tok, container, false);
            viewHolder = new ViewHolder(view);
            container.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) container.getTag();
        }

        NewsInfo item = mVideoBeans.get(position);

        // 开始预加载
        PreloadManager.getInstance(context).addPreloadTask(item.getVideoUrl(), position);
        viewHolder.mTitle.setText(item.getTitle());
        viewHolder.mAuthor.setText("@" + item.getAuthor());
        viewHolder.mDesc.setText(item.getContent());
        viewHolder.mLike.setText(item.getLikes());
        Glide.with(context)
                .load("https://picsum.photos/id/" + item.getImgUrl() + "/600/300")
                .into(viewHolder.mThumb);
        viewHolder.mTitle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

            }
        });
        viewHolder.mDlComment.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new XPopup.Builder(mContext)
                        .moveUpToKeyboard(false) // 如果不加这个，评论弹窗会移动到软键盘上面
                        .enableDrag(true)
                        .asCustom(new ZhihuCommentPopup(mContext, item.getTitle()))
                        .show();
            }
        });

        viewHolder.mUp.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                item.setLikes("" + (Integer.parseInt(item.getLikes()) - 1));
                notifyDataChanged();
            }
        });
        viewHolder.mDown.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                item.setLikes("" + (Integer.parseInt(item.getLikes()) + 1));
                notifyDataChanged();
            }
        });
        viewHolder.mPosition = position;
        container.addComponent(view);
        return view;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
        Component itemView = (Component) object;
        container.removeComponent(itemView);
        NewsInfo item = mVideoBeans.get(position);

        // 取消预加载
        PreloadManager.getInstance(container.getContext()).removePreloadTask(item.getVideoUrl());

        // 保存起来用来复用
        mViewPool.add(itemView);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    /**
     * 借鉴ListView item复用方法
     *
     * @since 2021-05-12
     */
    public static class ViewHolder {
        /**
         * 角标
         */
        public int mPosition;
        /**
         * 标题
         */
        public Text mTitle;
        /**
         * 作者
         */
        public Text mAuthor;
        /**
         * 作者
         */
        public Text mLike;
        /**
         * 描述
         */
        public Text mDesc;
        /**
         * 封面图
         */
        public Image mThumb;
        /**
         * 点赞按钮
         */
        private Image mUp, mDown;
        /**
         * TikTokView
         */
        public TikTokView mTikTokView;
        /**
         * StackLayout
         */
        public StackLayout mPlayerContainer;

        public DirectionalLayout mDlComment, mDlPush;

        /**
         * 构造函数
         *
         * @param itemView itemView
         */
        ViewHolder(Component itemView) {
            mTikTokView = (TikTokView) itemView.findComponentById(ResourceTable.Id_tiktok_View);
            mTitle = (Text) mTikTokView.findComponentById(ResourceTable.Id_tv_title);
            mAuthor = (Text) mTikTokView.findComponentById(ResourceTable.Id_tv_author);
            mDesc = (Text) mTikTokView.findComponentById(ResourceTable.Id_tv_desc);
            mLike = (Text) mTikTokView.findComponentById(ResourceTable.Id_tv_like);
            mThumb = (Image) mTikTokView.findComponentById(ResourceTable.Id_iv_thumb);
            mUp = (Image) mTikTokView.findComponentById(ResourceTable.Id_dl_up);
            mDown = (Image) mTikTokView.findComponentById(ResourceTable.Id_dl_down);
            mDlComment = (DirectionalLayout) mTikTokView.findComponentById(ResourceTable.Id_dl_comment);
            mDlPush = (DirectionalLayout) mTikTokView.findComponentById(ResourceTable.Id_dl_push);
            mPlayerContainer = (StackLayout) itemView.findComponentById(ResourceTable.Id_container);
            itemView.setTag(this);
        }
    }
}
