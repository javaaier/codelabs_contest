package com.xwg.demo.topvideo;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"},0);
        super.onStart(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
